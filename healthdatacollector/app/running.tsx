import { useRouter, useLocalSearchParams } from 'expo-router';
import React, { useEffect, useState } from 'react';
import { ImageBackground, View, Text, Button, StyleSheet,Alert } from 'react-native';
import * as SQLite from 'expo-sqlite';
//import * as Fitbitapicall from'./../WebAPICall/fitbitapicall';
import * as Config from './../WebAPICall/config';
import NetInfo from '@react-native-community/netinfo';

interface ApiError {
  error: string;
  // Add other properties that your API error returns
}

const App: React.FC = () => {
  const router = useRouter();
  const params = useLocalSearchParams();
  const { participantId } = params; // Get the 'user' parameter from the router
  const pIDforDB = typeof participantId === 'string' ? participantId : '';
  console.log('Params:', params);
  console.log('User:', participantId);
  const testgruppe = Math.floor(Math.random() * 6) + 1
  console.log('Testgruppe:', testgruppe);

  const handleButtonPress = async () => {
    // Implement the desired action when the button is pressed
    console.log('Button pressed!');

    // Create a timestamp der für den Start der Session benötigt wird
    const timestamp = new Date().toISOString();
    console.log('StartingTimestamp:', timestamp);

    // Open a connection to the SQLite database
    const db = await SQLite.openDatabaseAsync('participantData');

    // Create the table if it does not exist
    await db.execAsync(`
    PRAGMA journal_mode = WAL;
    CREATE TABLE IF NOT EXISTS participantData (id INTEGER PRIMARY KEY NOT NULL, participantID TEXT NOT NULL, startingTimestamp INTEGER, endTimestamp INTEGER, testgruppe INTEGER, heartrate STRING,distance STRING,steps STRING);
    `);

 
   // Aktueller Timestamp/ParticipantID/FitbitDaten wird in die Datenbank geschrieben-----------------------------------------------------------
    const result = await db.runAsync('INSERT INTO participantData (participantID, startingTimestamp, endTimestamp, testgruppe, heartrate,distance,steps) VALUES (?, ?, ?, ?, ?, ?, ?)', pIDforDB, timestamp, null, testgruppe,'','','');
    console.log("Result " +result.lastInsertRowId, result.changes);
     
     // Einfügen der JSON Dateien--------------------------------------------------------------------------------------------------------------- 
    
     
     // Abrufen des gerade eingefügten Datensatzes
     const lastInserted = await db.getFirstAsync<{ id: number; participantID: string; startingTimestamp: number; testgruppe: number;hearthrate:string,distance:string,steps:string }>('SELECT * FROM participantData WHERE id = ?', result.lastInsertRowId);

     if (lastInserted) {
       console.log('Gerade wurde folgendes eingefügt', lastInserted);
     } else {
       console.log('No rows found in the database.');
     } 

    // Navigate to the next screen
    router.push({pathname: 'soundplayer', params: {participantId: participantId, testgruppe: testgruppe}});
  };

  return (
    <ImageBackground
      source={require('../assets/images/designer.jpeg')}
      style={styles.background}
    >
      <View style={styles.content}>
        <View style={styles.whiteBox}>
          {participantId ? (
            <Text style={styles.greeting}>Hallo User {participantId}!</Text>
          ) : (
            <Text style={styles.greeting}>Hallo User!</Text>
          )}
          <Button title="RUN!" onPress={handleButtonPress} />
        </View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  whiteBox: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    padding: 20,
    borderRadius: 10,
  },
  background: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  greeting: {
    fontSize: 24,
    marginBottom: 20,
  },
});

export default App;
