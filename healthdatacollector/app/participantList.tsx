// ParticipantList.tsx
import React, { useState, useCallback } from 'react';
import { View, Text, FlatList, StyleSheet, Alert, Dimensions, TouchableOpacity } from 'react-native';
import * as SQLite from 'expo-sqlite';
import { useFocusEffect } from '@react-navigation/native';
import { RectButton, Swipeable } from 'react-native-gesture-handler';
import { router } from 'expo-router';

interface ParticipantData {
  id: number;
  participantID: string;
  startingTimestamp: number;
  endTimestamp: number;
}

const ParticipantList: React.FC = () => {
  const [entries, setEntries] = useState<ParticipantData[]>([]);
  const [dbError, setDbError] = useState<boolean>(false);

  const fetchEntries = async () => {
    try {
      const db = await SQLite.openDatabaseAsync('participantData');
      // Create the table if it does not exist
      await db.execAsync(`
      PRAGMA journal_mode = WAL;
      CREATE TABLE IF NOT EXISTS participantData (id INTEGER PRIMARY KEY NOT NULL, participantID TEXT NOT NULL, startingTimestamp INTEGER, endTimestamp INTEGER, testgruppe INTEGER, heartrate STRING,distance STRING,steps STRING);
      `);
      const result = await db.getAllAsync<ParticipantData>('SELECT * FROM participantData');
      console.log('Result:', result);
      setEntries(result);
    } catch (error) {
      console.error(error);
      setDbError(true);
    }
  };

  useFocusEffect(
    useCallback(() => {
      fetchEntries();
    }, [])
  );

  const deleteEntry = async (id: number) => {
    try {
      const db = await SQLite.openDatabaseAsync('participantData');
      await db.runAsync('DELETE FROM participantData WHERE id = ?', id);
      setEntries((prevEntries) => prevEntries.filter((entry) => entry.id !== id));
    } catch (error) {
      console.error(error);
      Alert.alert('Error', 'Could not delete the entry from the database.');
    }
  };

  const renderRightActions = (id: number) => (
    <RectButton style={styles.deleteButton} onPress={() => deleteEntry(id)}>
      <Text style={styles.deleteButtonText}>Delete</Text>
    </RectButton>
  );

  const formatTimestamp = (timestamp: number) => {
    const date = new Date(timestamp);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Monate sind nullbasiert
    const year = date.getFullYear();
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
  
    return `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
  
  };
  const handlePress = (id: number, startingTimestamp: number, endTimestamp: number) => {
    console.log('Detailseite soll geöffnet werden.');
    // Navigate to the next screen
    router.push({pathname: 'detailHistory', params: {id: id, startingTimestamp: startingTimestamp, endTimestamp: endTimestamp}});
  }

  const renderItem = ({ item }: { item: ParticipantData }) => (
    <Swipeable renderRightActions={() => renderRightActions(item.id)}>
      <View style={styles.swipeableContainer}>
        <TouchableOpacity onPress={() => handlePress(item.id, item.startingTimestamp, item.endTimestamp)}>
            <View style={styles.item}>
                <Text style={styles.itemText}>ParticipantID: {item.participantID}</Text>
                <Text style={styles.itemText}>Start: {formatTimestamp(item.startingTimestamp)}</Text>
                <Text style={styles.itemText}>Ende: {formatTimestamp(item.endTimestamp)}</Text>
            </View>
        </TouchableOpacity>
      </View>
    </Swipeable>
  );

  if (dbError) {
    return (
      <View style={styles.container}>
        <Text style={styles.errorText}>Error loading data from database.</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {entries.length === 0 ? (
        <Text style={styles.emptyText}>No entries found in the database.</Text>
      ) : (
        <FlatList
          data={entries}
          keyExtractor={(item) => item.id.toString()}
          renderItem={renderItem}
          contentContainerStyle={styles.flatListContent}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
    itemText:{
        color: 'white',
    },
    container: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 0,
      },
      flatListContent: {
        width: '100%',
        paddingVertical: 8,
      },
      item: {
        padding: 10,
        marginVertical: 8,
        backgroundColor: '#273e14',
        borderRadius: 5,
        width: '100%',
      },
      emptyText: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: 20,
      },
      errorText: {
        fontSize: 18,
        color: 'red',
        textAlign: 'center',
        marginTop: 20,
      },
      deleteButton: {
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-end',
        padding: 20,
        marginVertical: 8,
        borderRadius: 5,
      },
      deleteButtonText: {
        color: 'white',
        fontWeight: 'bold',
      },
      swipeableContainer: {
        width: '100%',
      },
});

export default ParticipantList;
