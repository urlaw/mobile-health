import React, { useState, useEffect } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { Audio } from 'expo-av';
import { router, useLocalSearchParams } from 'expo-router';
import soundMap from '../components/soundmap';
import { Ionicons } from '@expo/vector-icons';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import * as SQLite from 'expo-sqlite';

const SoundPlayer: React.FC = () => {
    const [track, setTrack] = useState<string>(''); // Track name
    const [progress, setProgress] = useState<number>(0); // Track progress in percentage
    const [isPlaying, setIsPlaying] = useState<boolean>(true); // Track playing state
    const [sound, setSound] = useState<Audio.Sound | undefined>();
    const params = useLocalSearchParams();
    console.log('Params:', params);
    const testgruppe = params.testgruppe ?? '1' // Get the 'user' parameter from the router
    var tracknummer: number = +testgruppe;
    console.log('Tracknummer:', tracknummer);
    const { participantId } = params; // Get the 'user' parameter from the router
    const pIDforDB = typeof participantId === 'string' ? participantId : '';
    

    // // Fetch the selected track from the router or any other source
    // useEffect(() => {
    //     const selectedTrack = 'Track 1';
    //     setTrack(selectedTrack);
    // }, []);

    useEffect(() => {
        async function loadSound() {
            console.log('Loading Sound');
            console.log('Tracknummer:', tracknummer);

            // Use the soundMap to get the correct sound file
            const soundFile = soundMap[tracknummer];
            if (soundFile) {
                const { sound } = await Audio.Sound.createAsync(soundFile);
                setSound(sound);
                console.log('Playing Sound');
                await sound.playAsync();

                // This will keep the sound playing automatically when component mounts
                setIsPlaying(true);

                // Set interval to update progress
                const interval = setInterval(async () => {
                    const status = await sound.getStatusAsync();
                    if (status.isLoaded && status.durationMillis) {
                        setProgress((status.positionMillis / status.durationMillis) * 100);
                    }
                }, 1000);
            } else {
                console.error('Sound file not found for tracknummer:', tracknummer);
            }
        }

        loadSound();

        return () => {
            // Clean up the sound object when the component unmounts
            if (sound) {
                console.log('Unloading Sound');
                sound.unloadAsync();
            }
        };
    }, [tracknummer]); // Dependency array includes tracknummer
    // useEffect(() => {
    //     async function loadSound() {
    //         console.log('Loading Sound');
    //         console.log('Tracknummer:', tracknummer);
    //         const soundModule = await import('../assets/sound/'+tracknummer+'.mp3');
    //         const { sound } = await Audio.Sound.createAsync(soundModule.default);
    //         setSound(sound);
    //         console.log('Playing Sound');
    //         await sound.playAsync();

    //         // This will keep the sound playing automatically when component mounts
    //         setIsPlaying(true);
    //     }

    //     loadSound();

    //     return () => {
    //         // Clean up the sound object when the component unmounts
    //         if (sound) {
    //             console.log('Unloading Sound');
    //             sound.unloadAsync();
    //         }
    //     };
    // }, []);

    // Function to handle play/pause button click
    const handlePlayPause = async () => {
        console.log('Play/Pause Clicked');
        if (sound) {
            if (isPlaying) {
                await sound.pauseAsync();
            } else {
                await sound.playAsync();
            }
            setIsPlaying(!isPlaying);
        }
    };

    // Function to handle stop button click
    const handleStop = async () => {
        Alert.alert(
            "Session beenden",
            "Möchten Sie die Session wirklich beenden?",
            [
                {
                    text: "Abbrechen",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "Ja", onPress: async () => {
                        // Open a connection to the SQLite database and update the endTimestamp
                        const db = await SQLite.openDatabaseAsync('participantData');
                        const timestamp = new Date().toISOString();
                        console.log('EndTimestamp:', timestamp);
                        const result = await db.runAsync('UPDATE participantData SET endTimestamp = ? WHERE participantID = ?', timestamp, pIDforDB);
                        
                        // DEBUGGING: Überprüfung ob der Datensatz in der Datenbank aktualisiert wurde
                        const lastInserted = await db.getFirstAsync<{ id: number; participantID: string; startingTimestamp: number; testgruppe: number }>('SELECT * FROM participantData WHERE id = ?', result.lastInsertRowId);
                        if (lastInserted) {
                        console.log('Gerade wurde folgendes aktualisiert', lastInserted);
                        } else {
                        console.log('No rows found in the database.');
                        }

                        // Clean up the sound object when the component unmounts and navigate to the endscreen
                        console.log('Stop Clicked');
                        if (sound) {
                            await sound.stopAsync();
                            sound.unloadAsync();
                        }
                        router.replace({pathname: 'endscreen', params: {participantId: participantId}});
                    }
                }
            ]
        );
    };

    return (
        <View style={styles.container}>
            <Text style={styles.trackName}>Tracknummer {tracknummer}</Text>
            <AnimatedCircularProgress
                size={100}
                width={10}
                fill={progress}
                tintColor="#76c7c0"
                backgroundColor="#e0e0e0"
            >
                {() => <Text>{`${Math.round(progress)}%`}</Text>}
            </AnimatedCircularProgress>
            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={handlePlayPause} style={styles.buttonType}>
                    <Ionicons name={isPlaying ? "pause" : "play"} size={24} color="white" />
                </TouchableOpacity>
                <TouchableOpacity onPress={handleStop} style={styles.buttonType}>
                    <Ionicons name="square" size={24} color="white" />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgb(39, 62, 20)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
    },
    trackName: {
        margin: 20,
        fontSize: 28,
        fontWeight: 'bold',
        color: 'white',
    },
    buttonType: {
        backgroundColor: 'rgb(0, 128, 0)',
        padding: 10,
        borderRadius: 5,
        margin: 10,
    },
});

export default SoundPlayer;
