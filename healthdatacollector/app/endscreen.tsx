import React, { useEffect, useState } from 'react';
import { View, Text, Button, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { useLocalSearchParams, useNavigation, useRouter } from 'expo-router';
import * as SQLite from 'expo-sqlite';

const MissionScreen: React.FC = () => {
    const router = useRouter();
    const params = useLocalSearchParams();
    console.log('Params:', params);
    const { participantId } = params; // Get the 'user' parameter from the router
    const pIDforDB = typeof participantId === 'string' ? participantId : '';
    const [duration, setDuration] = useState<string | null>(null);
    const [missionNumber, setMissionNumber] = useState<string | null>(null);
    const navigation = useNavigation();

    React.useEffect(
        () =>
          navigation.addListener('beforeRemove', (e) => {
            
    
            // Prevent default behavior of leaving the screen
            e.preventDefault();

            // Prompt the user before leaving the screen
            Alert.alert(
              'Zum Startbildschirm zurückkehren?',
              'Die Session wird beendet und die Daten werden gespeichert. Möchten Sie fortfahren?',
              [
                { text: "Don't leave", style: 'cancel', onPress: () => {} },
                {
                  text: 'Discard',
                  style: 'destructive',
                  // If the user confirmed, then we dispatch the action we blocked earlier
                  // This will continue the action that had triggered the removal of the screen
                  onPress: () => navigation.dispatch(e.data.action),
                },
              ]
            );
          }),
        [navigation]
      );

    // Get Duration from the database
    const getDuration = async () => {
        const db = await SQLite.openDatabaseAsync('participantData');
        const lastInserted = await db.getFirstAsync<{ id: number; participantID: string; startingTimestamp: number; testgruppe: number; endTimestamp: number }>('SELECT * FROM participantData WHERE participantID = ?', pIDforDB);
        
        if (lastInserted) {
            const startTimestamp = new Date(lastInserted.startingTimestamp);
            const endTimestamp = new Date(lastInserted.endTimestamp);
            setMissionNumber(lastInserted.testgruppe.toString());
            // Überprüfen, ob die resultierenden Date-Objekte gültig sind
            if (!isNaN(startTimestamp.getTime()) && !isNaN(endTimestamp.getTime())) {
                const durationMillis = endTimestamp.getTime() - startTimestamp.getTime();
                const minutes = Math.floor(durationMillis / 60000);
                const seconds = Math.floor((durationMillis % 60000) / 1000);
                setDuration(`${minutes}m ${seconds}s`);
                console.log("Duration: " + durationMillis + " milliseconds");
            } else {
                console.log("Invalid Date objects: Start - " + startTimestamp + ", End - " + endTimestamp);
            }
        } else {
            console.log("lastInserted is null: " + lastInserted);
        }
        
    };
    // Call the function
    getDuration();

    const handleEndMission = () => {
        //Navigiert zurück zur Startseite
        router.dismissAll();
    };

    

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerText}>MISSION {missionNumber}</Text>
                <Text style={styles.description}>In the beautiful streets of Blue Rockshore is a famous market taking place today. But not everyone is acting fairly...</Text>
                <TouchableOpacity style={styles.startButton} onPress={handleEndMission}>
                    <Text style={styles.startButtonText}>Start mission</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.resultContainer}>
                <View style={styles.resultBox}>
                    <Text style={styles.resultText}>Duration</Text>
                    {duration && <Text style={styles.resultValue}>{duration}</Text>}
                </View>
                <View style={styles.resultBox}>
                    <Text style={styles.resultText}>Distance</Text>
                </View>
                <View style={styles.resultBox}>
                    <Text style={styles.resultText}>Speed</Text>
                </View>
                <View style={styles.resultBox}>
                    <Text style={styles.resultText}>Intervals</Text>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(39, 62, 20)',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 20,
    },
    header: {
        alignItems: 'center',
        marginHorizontal: 20,
    },
    headerText: {
        fontSize: 28,
        fontWeight: 'bold',
        color: 'white',
        marginBottom: 10,
    },
    description: {
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
        marginBottom: 20,
    },
    startButton: {
        backgroundColor: 'rgb(0, 128, 0)',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
    },
    startButtonText: {
        fontSize: 18,
        color: 'white',
    },
    resultContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: 20,
    },
    resultBox: {
        backgroundColor: 'rgb(0, 128, 0)',
        padding: 10,
        borderRadius: 5,
        width: '22%',
        alignItems: 'center',
    },
    resultText: {
        fontSize: 10,
        color: 'white',
    },
    resultValue: {
        fontSize: 12,
        color: 'white',
        marginTop: 5,
    },
});

export default MissionScreen;
