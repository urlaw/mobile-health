import { Image, StyleSheet, Platform, Button, Alert, TextInput } from 'react-native';

import { HelloWave } from '@/components/HelloWave';
import ParallaxScrollView from '@/components/ParallaxScrollView';
import { ThemedText } from '@/components/ThemedText';
import { ThemedView } from '@/components/ThemedView';
import { useState } from 'react';
import { router } from 'expo-router';
//import { authorize, refresh } from 'react-native-app-auth';


export default function HomeScreen() {
  const [inputValue, setInputValue] = useState('');
  
  /*const config = {
    clientId: '23PF5D',
    clientSecret: '8ae8ccacbba1aba2c06a3913182b7acd',
    redirectUrl: 'https://localhost',
    scopes: ['activity', 'heartrate', 'profile'], // Je nachdem, welche Bereiche du benötigst
    serviceConfiguration: {
      authorizationEndpoint: 'https://www.fitbit.com/oauth2/authorize',
      tokenEndpoint: 'https://api.fitbit.com/oauth2/token',
      revocationEndpoint: 'https://api.fitbit.com/oauth2/revoke',
    },
  };
  
  // Funktion zum Starten der Authentifizierung
  const startAuth = async () => {
    try {
      const authState = await authorize(config);
      console.log('Auth State:', authState);
      // Hier kannst du den Access Token und andere Informationen speichern
      // z.B. AsyncStorage oder Redux Store
    } catch (error) {
      console.log('Auth Error:', error);
    }
  };*/


  console.log('HomeScreen rendered');
  
  const handleInputChange = (text: string) => {
    setInputValue(text.replace(/[^0-9]/g, ''));
  };

  const handleButtonPress = () => {
    const number = parseInt(inputValue);
    if (!isNaN(number)) {
      
      // Navigate to new page based on the entered number
      const param = number.toString();
      //router.setParams({participantId: param});
      router.push({pathname: 'running', params: {participantId: number}});
      //router.navigate('running');
    } else {
      Alert.alert('Invalid Input', 'Please enter a valid number');
    }
  };

  const handleButtonPress2 = () => {
    //startAuth();
  };

  return (
    <ParallaxScrollView
      headerBackgroundColor={{ light: '#A1CEDC', dark: '#1D3D47' }}
      headerImage={
        <Image
          source={require('@/assets/images/partial-react-logo.png')}
          style={styles.reactLogo}
        />
      }>
      <ThemedView style={styles.titleContainer}>
        <ThemedText type="title">Welcome!</ThemedText>        
      </ThemedView>
      <ThemedView style={styles.stepContainer}>
        <ThemedText type="subtitle">DatenButton</ThemedText>
        <TextInput
          value={inputValue}
          onChangeText={handleInputChange}
          keyboardType="numeric"
          placeholder="Enter a number"
          style={styles.input}
        />
        <Button
          onPress={handleButtonPress}
          title="Enter Participant ID"
          color="#841584"
          accessibilityLabel="Navigate to new page"
        />
        <Button
          onPress={handleButtonPress2}
          title="Authorize Fitbit"
          color="#841584"
          accessibilityLabel="Navigate to new page"
        />
      </ThemedView>
      
    </ParallaxScrollView>
  );
}

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 8,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    paddingLeft: 10,
    paddingRight: 10,
    width: '100%',
    backgroundColor: '#fff',
    color: '#000',
  },
  stepContainer: {
    gap: 8,
    marginBottom: 8,
  },
  reactLogo: {
    height: 178,
    width: 290,
    bottom: 0,
    left: 0,
    position: 'absolute',
  },
});