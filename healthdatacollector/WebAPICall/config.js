const FITBIT_REDIRECT_URL = 'redirect_uri=http://localhost:5000/form112'; // Needs to point back at the server running the application. Needs to be the same as Callback URL in fitbit api settings https://dev.fitbit.com/
    const FITBIT_CLIENT_ID = '23PDSW';
    const FITBIT_CLIENT_SECRET = '2600ac346c6bac4eaf79d3a95723ff90';
    const FITBIT_OAUTH_URL = 'https://www.fitbit.com/oauth2/authorize?';
    const FITBIT_ACCOUNT_CREATION = '2024-07-07 00:00:00'; // Day where we start looking for logged data
    // Access Token which lasts for 1 Year
    const ACCESS_TOKEN ='eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyM1BHQ0wiLCJzdWIiOiJDM1hKM1YiLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJzY29wZXMiOiJyc29jIHJlY2cgcnNldCByb3h5IHJudXQgcnBybyByc2xlIHJjZiByYWN0IHJsb2MgcnJlcyByd2VpIHJociBydGVtIiwiZXhwIjoxNzUyNTEwOTk3LCJpYXQiOjE3MjA5NzQ5OTd9.C_ijUN7NzxhMs7mf4FqQAnADHhHksP060NdKSHzfFAU';
    module.exports = {
        FITBIT_OAUTH_URL: FITBIT_OAUTH_URL,
        ACCESS_TOKEN,
        FITBIT_REDIRECT_URL: FITBIT_REDIRECT_URL,
        FITBIT_CLIENT_ID: FITBIT_CLIENT_ID,
        FITBIT_CLIENT_SECRET: FITBIT_CLIENT_SECRET,
        FITBIT_ACCOUNT_CREATION: FITBIT_ACCOUNT_CREATION,
      };