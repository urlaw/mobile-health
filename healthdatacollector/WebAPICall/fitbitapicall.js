import React, { useEffect, useState } from 'react';
import { ImageBackground, View, Text, Button, StyleSheet,Alert } from 'react-native';

import Config from './config';
console.log(Config.ACCESS_TOKEN);

// Get Fitness Data within this timestamp

async function getheartData(access_token,day,start_time,end_time) {
 // Check Internetconnection
  // Send Alert
  // Built request Link

  try {
    console.log('Entered Try block');
    const FULLURL = 'https://api.fitbit.com/1/user/-/activities/heart/date/2024-07-10/2024-07-10/1sec/time/14:00/14:01.json';

    console.log("Entered try");
    const response = await fetch(FULLURL, {
      method: 'GET',
      headers: {Authorization: 'Bearer ' + access_token},
    });
    if (!response.ok) {
      throw new Error('Network response was not ok');
    }
    const jsonData = await response.json();
    console.log("Heartrate from Function" + jsonData);
    return(jsonData); 
  } catch (error) {
    return {data: null, error: error.message};
  }
};

// Get the distance
async function getdistanceData(access_token,start_time,end_time) {
  // Check Internetconnection
   console.log('Entered getheartData');
   // Send Alert
   // Built request Link
   const FULLURL2 =
     'https://api.fitbit.com/1/user/-/activities/distance/date/today/1d/1min.json';
   console.log(FULLURL2);
   console.log('Short before fetch');
   try {
     const response = await fetch(FULLURL2, {
       method: 'GET',
       headers: {Authorization: 'Bearer ' + access_token},
     });
 
     console.log('Function Response' + JSON.stringify(response));
     if (!response.ok) {
       throw new Error('Network response was not ok');
     }
     const jsonData = await response.json();
     return jsonData;
   } catch (error) {
     return {data: null, error: error.message};
   }
 };

 async function getstepsData(access_token,day,start_time,end_time) {
  // Check Internetconnection
   console.log('Entered getheartData');
   
   // Send Alert
   // Built request Link
   const FULLURL3 ='https://api.fitbit.com/1/user/-/activities/steps/date/today/1d/1min.json';
   console.log(FULLURL3);
   console.log('Short before fetch');

   try {
     const response = await fetch(FULLURL3, {
       method: 'GET',
       headers: {Authorization: 'Bearer ' + access_token},
     });
 
     console.log('Function Response' + JSON.stringify(response));
     if (!response.ok) {
       throw new Error('Network response was not ok');
     }
     const jsonData = await response.json();
     return jsonData;
   } catch (error) {
     return {data: null, error: error.message};
   }
 };

 export {getheartData,getdistanceData,getstepsData};
