const soundMap: { [key: string]: any } = {
    '1': require('../assets/sound/1.mp3'),
    '2': require('../assets/sound/2.mp3'),
    '3': require('../assets/sound/3.mp3'),
    '4': require('../assets/sound/4.mp3'),
    '5': require('../assets/sound/5.mp3'),
    '6': require('../assets/sound/6.mp3'),
};

export default soundMap;